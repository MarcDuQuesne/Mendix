## Intro

The purpose of this challenge was to create a project and write a bunch of tests for the `/api/articles` endpoint of a given application. The project is written in python, making use of the `pytest` framework, `pydantic` for validating the json schemas and `requests` for interacting with the API.

# Install

This project requires `python>=3.9`. A conda env file is available to install an appropriate environment:

`conda env create -f mendix_api_tests.yml`

To install the project and its dependencies use the provided `setup.py`:

`pip install -e src/`

a dockerfile is also available in `docker/` and it is used by the Gitlab CI/CD pipeline (see below).
The app we want to test was also dockerized, and the image was pushed to the gitlab registry of this project.

# Run

To tun the tests, define the env variable `API_HOST` and `API_PORT` and run

`pytest src/tests`

or run:

`docker-compose up` 

the latter will 

- start the app in a docker container and expose port 9000.
- wait some seconds and start the tests in a second docker image.

it is of course possible to start just one of the two.

# CI/CD

I designed a CI/CD pipeline that 

- builds the new docker image for the test project and pushes it to the gitlab registry
- checks the quality of the code for the test project itself. The pylint score and test coverage are reported as badges.
- runs the tests for the test project.
- runs the tests for the app, by running it in docker as a gitlab service. 
