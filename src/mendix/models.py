"""
data models
"""

from typing import Optional, List
from datetime import datetime
import re
from pydantic import validator, Extra, HttpUrl

from mendix.basemodel import BaseModel


class User(BaseModel):
    """
    Class to store and validate a string representing the species and size of a flower.
    """
    email: Optional[str]
    token: Optional[str] = None
    username: Optional[str]
    bio: Optional[str] = None
    image: Optional[HttpUrl] = None
    password: Optional[str] = None

    class Config:
        extra = Extra.forbid

    @validator('email')
    @classmethod
    def check_email_pattern(cls, value):
        """
        Emails should follow a given pattern
        """

        email_patten = r".+@.+\..+"

        if not re.match(email_patten, value):
            raise ValueError('The Email does not appear to follow an email pattern.')
        return value


class Author(BaseModel):
    """
    Author
    """
    username: str
    bio: Optional[str]
    image: Optional[HttpUrl]
    following: bool

    class Config:
        extra = Extra.forbid


class Article(BaseModel):
    """
    Article
    """
    slug: Optional[str]
    title: str
    description: str
    body: str
    tagList: Optional[List[str]]
    createdAt: Optional[datetime]
    updatedAt: Optional[datetime]
    favorited: Optional[bool]
    favoritesCount: Optional[int]
    author: Optional[Author]

    class Config:
        extra = Extra.forbid


class Articles(BaseModel):
    """
    Articles.
    """
    articles: List[Article]
    articlesCount: int

    class Config:
        extra = Extra.forbid


class Error(BaseModel):
    """
    Error
    """
    body: List[str]

    class Config:
        extra = Extra.forbid

class Comment(BaseModel):
    """
    Comment
    """
    id: int
    createdAt: Optional[datetime]
    updatedAt: Optional[datetime]
    body: str
    author: Optional[Author]

    class Config:
        extra = Extra.forbid

class Comments(BaseModel):
    """
    Comments
    """

    comments: List[Optional[Comment]]

    class Config:
        extra = Extra.forbid
