import pytest
import requests
from jsonschema import validate
import uuid
import logging

from mendix import config
from mendix.models import User, Error

logger = logging.getLogger(__file__)

HEADERS = {
    'Content-type': 'application/json; charset=utf-8'
}

DEFAULT_USER = {"username": "Jacob",
                "email": "jake@jake.jake",
                "password": "jakejake"}

OTHER_USER = {"username": "Jacob2",
              "email": "jake2@jake2.jake2",
              "password": "jake2jake2"}

def generate_random_string():
    """
    Just a random string to make fields unique.
    """
    return uuid.uuid4().hex[:6].lower()


@pytest.fixture
def ensure_user_exists(user, autouse=True, scope="session"):
    """
    Add a (default) user to the application.
    """

    if not user: return

    response = requests.post(f"{config.API_URL}/users", json={'user': user})
    assert response.status_code == 201 or response.status_code == 409

    if response.status_code == 201:
        assert 'user' in response.json().keys()
        validate(response.json()['user'], User.schema())
    elif response.status_code == 409:
        assert 'errors' in response.json().keys()
        validate(response.json()['errors'], Error.schema())

    logger.info(f'Added user {user["username"]}')


def initialize_session(user):
    """
    Initialize a session and optionally logs in a user.
    """

    session = requests.Session()
    session.headers.update(HEADERS)

    if user is not None:

        _user = {'email': user['email'],
                 'password': user['password']}

        response = session.post(f"{config.API_URL}/users/login", json={'user': _user})
        assert response.status_code == 200
        assert 'user' in response.json().keys()
        validate(response.json()['user'], User.schema())

        token = response.json()['user']['token']
        session.headers.update({'Authorization': f'Token {token}'})

    return session

def post_article(session):
    # only unique articles.
    article = {"title": "How to train your dragon" + generate_random_string(),
               "description": "Ever wonder how?" + generate_random_string(),
               "body": "You have to believe" + generate_random_string(),
               "tagList": ["reactjs", "angularjs", "dragons"]}

    response = session.post(f"{config.API_URL}/articles", json={"article": article})
    return response

@pytest.fixture
def post_comment(session, post_article):

    response = post_article
    slug = response.json()['article']['slug']
    # only unique articles.
    comment = {"body": "this is a comment" + generate_random_string()}
    response = session.post(f"{config.API_URL}/articles/{slug}/comments", json={"comment": comment})
    yield response