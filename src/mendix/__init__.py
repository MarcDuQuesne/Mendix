"""
Project config
"""

from pathlib import Path
from typing import Optional
from pydantic import BaseSettings, AnyUrl, validator

ROOT = Path(__file__).parents[2]


class Config(BaseSettings):
    """App config"""

    API_HOST: str = 'localhost'
    API_PORT: str = '9000'

    API_URL: Optional[str]

    @validator("API_URL", pre=True)
    @classmethod
    def assemble_api_url(cls, url, values) -> str:
        """
        validate/generate db url
        """
        return f"http://{values['API_HOST']}:{values['API_PORT']}/api"

    class Config:
        env_file = ROOT / ".env"


config = Config()
