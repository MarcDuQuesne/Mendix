from setuptools import setup, find_packages

with open('requirements.txt') as f:
  required = f.read().splitlines()

setup(
    name='mendix_test_api',
    version='0.0.1',
    license='',
    description='',
    long_description="",
    long_description_content_type='text/markdown',
    author='Matteo Giani',
    packages=find_packages(),
    python_requires='>=3.9',
    # please see the requirements.txt
    install_requires=required,
    entry_points={}
)
