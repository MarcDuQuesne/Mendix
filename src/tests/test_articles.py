"""
Test the /api/articles entrypoint:

 - GET /api/articles, with or without authentication and with options
 - GET /api/articles/feed, with or without authentication and with options
 - POST /api/articles, with or without authentication
 - PUT /api/articles/:slug, with or without authentication
 - DELETE /api/articles/:slug, with or without authentication

 - GET /api/articles/:slug/comments, with or without authentication
 - POST /api/articles/:slug/comments, with or without authentication
 - DELETE /api/articles/:slug/comments/:id, with or without authentication
"""

# TODO apparently once a user is logged in he is able to delete/update someone else's articles/comments.
# is this intended behavior?

import pytest
from jsonschema import validate

from mendix import config
from mendix.models import Article, Articles, Comment, Comments
from mendix.utils import HEADERS, DEFAULT_USER, OTHER_USER, ensure_user_exists, initialize_session, generate_random_string, post_article, post_comment


@pytest.mark.parametrize("user", [None, DEFAULT_USER])
@pytest.mark.parametrize("option", ['', 'author=jake', 'tag=AngularJS', 'limit=20', 'offset=0', 'offset=0&author=jake'])
def test_get_articles(ensure_user_exists, user, option):

    session = initialize_session(user)
    response = session.get(f"{config.API_URL}/articles?{option}")
    assert response.status_code == 200 and response.headers['content-type'] == 'application/json;charset=utf-8'
    validate(response.json(), Articles.schema())


@pytest.mark.parametrize("user", [None, DEFAULT_USER])
@pytest.mark.parametrize("option", ['', 'limit=0', 'offset=0'])
def test_get_articles_feed(ensure_user_exists, user, option):

    session = initialize_session(user)
    response = session.get(f"{config.API_URL}/articles/feed?{option}")

    if user is not None:
        assert response.status_code == 200
        validate(response.json(), Articles.schema())
    else:
        assert response.status_code == 401

@pytest.mark.parametrize("user", [None, DEFAULT_USER])
def test_post_articles(ensure_user_exists, user):

    session = initialize_session(user)
    response = post_article(session)

    if user is None:
        assert response.status_code == 401
    else:
        assert response.status_code == 201
        assert 'article' in response.json()
        validate(response.json()['article'], Article.schema())

@pytest.mark.parametrize("user", [None, DEFAULT_USER])
def test_post_comment(ensure_user_exists, user):

    _session = initialize_session(DEFAULT_USER)
    response = post_article(_session)
    slug = response.json()['article']['slug']

    session = initialize_session(user)
    # only unique articles.
    comment = {"body": "this is a comment" + generate_random_string()}
    response = session.post(f"{config.API_URL}/articles/{slug}/comments", json={"comment": comment})

    if user is None:
        assert response.status_code == 401
    else:
        assert response.status_code == 200
        assert 'comment' in response.json()
        validate(response.json()['comment'], Comment.schema())

@pytest.mark.parametrize("user", [None, DEFAULT_USER])
def test_get_comments(ensure_user_exists, user):

    _session = initialize_session(DEFAULT_USER)
    response = post_article(_session)
    slug = response.json()['article']['slug']
    comment = {"body": "this is a comment" + generate_random_string()}
    response = _session.post(f"{config.API_URL}/articles/{slug}/comments", json={"comment": comment})

    session = initialize_session(user)
    # only unique articles.
    response = session.get(f"{config.API_URL}/articles/{slug}/comments")

    assert response.status_code == 200
    assert 'comments' in response.json()
    validate(response.json(), Comments.schema())

@pytest.mark.parametrize("user", [None, DEFAULT_USER, OTHER_USER])
def test_update_article(ensure_user_exists, user):

    _session = initialize_session(DEFAULT_USER)
    response = post_article(_session)
    slug = response.json()['article']['slug']

    updates = {
        "title": "Updated_title",
        "description": "Updated_description",
        "body": "Updated_body"
    }

    session = initialize_session(user)
    response = session.put(f"{config.API_URL}/articles/{slug}", json={"article": updates})

    if user == DEFAULT_USER:
        assert response.status_code == 200
        validate(response.json()['article'], Article.schema())
        assert response.json()['article']['title'] == 'Updated_title'
        assert response.json()['article']['description'] == 'Updated_description'
        assert response.json()['article']['body'] == 'Updated_body'
    elif user == OTHER_USER:
        pass
    else:
        assert response.status_code == 401


@pytest.mark.parametrize("user", [None, DEFAULT_USER, OTHER_USER])
def test_delete_article(ensure_user_exists, user):

    _session = initialize_session(DEFAULT_USER)
    response = post_article(_session)
    slug = response.json()['article']['slug']

    session = initialize_session(user)
    response = session.delete(f"{config.API_URL}/articles/{slug}")

    if user == DEFAULT_USER:
        assert response.status_code == 200
    elif user == OTHER_USER:
        pass
    else:
        assert response.status_code == 401

@pytest.mark.parametrize("user", [None, DEFAULT_USER, OTHER_USER])
def test_delete_comment(ensure_user_exists, user):

    _session = initialize_session(DEFAULT_USER)
    response = post_article(_session)
    slug = response.json()['article']['slug']
    comment = {"body": "this is a comment" + generate_random_string()}
    response = _session.post(f"{config.API_URL}/articles/{slug}/comments", json={"comment": comment})
    id = response.json()['comment']['id']

    session = initialize_session(user)
    response = session.delete(f"{config.API_URL}/articles/{slug}/comments/{id}")

    if user == DEFAULT_USER:
        assert response.status_code == 200
        response = session.get(f"{config.API_URL}/articles/{slug}/comments")
        assert id not in [comment['id'] for comment in response.json()['comments']]
    elif user == OTHER_USER:
        # MG what to expect here?
        pass
    else:
        assert response.status_code == 401
        response = session.get(f"{config.API_URL}/articles/{slug}/comments")
        assert id in [comment['id'] for comment in response.json()['comments']]
