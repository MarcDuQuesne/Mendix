
"""
Validate the schema of model instances.
"""

import pytest
from jsonschema import validate

from mendix.models import User, Article, Error, Comments

user_null_image = {
    "email": "jake@jake.jake",
    "token": "jwt.token.here",
    "username": "jake",
    "bio": "I work at statefarm",
    "image": None
}

user_str_image = {
    "email": "jake@jake.jake",
    "token": "jwt.token.here",
    "username": "jake",
    "bio": "I work at statefarm",
    "image": 'image'
}

article = {
    "slug": "how-to-train-your-dragon",
    "title": "How to train your dragon",
    "description": "Ever wonder how?",
    "body": "It takes a Jacobian",
    "tagList": ["dragons", "training"],
    "createdAt": "2016-02-18T03:22:56.637Z",
    "updatedAt": "2016-02-18T03:48:35.824Z",
    "favorited": False,
    "favoritesCount": 0,
    "author": {
      "username": "jake",
      "bio": "I work at statefarm",
      "image": "https://i.stack.imgur.com/xHWG8.jpg",
      "following": False
    }
  }

error = {
    "body": [
        "can't be empty"]
}

comment = {'author': {'bio': '',
           'following': False,
           'image': None,
           'username': 'Jacob'},
           'body': 'this is a comment9539bc',
           'createdAt': '2021-06-06T13:31:42.577Z',
           'id': 45,
           'updatedAt': '2021-06-06T13:31:42.577Z'}

comments = {'comments': [comment]}

cases = [
    (user_null_image, User),
    (user_str_image, User),
    (article, Article),
    (error, Error),
    (comments, Comments),
    ]


@pytest.mark.parametrize("instance, Model", cases)
def test_schema(instance, Model):
    """
    Validate the schema of model instances.
    """
    validate(instance=instance, schema=Model.schema())
